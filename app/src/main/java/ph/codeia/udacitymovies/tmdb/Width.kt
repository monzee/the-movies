package ph.codeia.udacitymovies.tmdb

import ph.codeia.udacitymovies.domain.Domain.NominalSize
import ph.codeia.udacitymovies.domain.Domain.Size

/**
 * This file is a part of the Udacity Movies project.
 */
enum class Width(override val tier: NominalSize, private val value: String) : Size {

    XS(NominalSize.TINY, "w92"),
    S(NominalSize.SMALL, "w154"),
    M(NominalSize.OTHER, "w185"),
    L(NominalSize.MEDIUM, "w342"),
    XL(NominalSize.OTHER, "w500"),
    XXL(NominalSize.LARGE, "w680"),
    FULL(NominalSize.XLARGE, "original");

    override fun toString(): String = value

}
