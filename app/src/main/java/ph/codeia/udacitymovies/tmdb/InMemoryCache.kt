package ph.codeia.udacitymovies.tmdb

import ph.codeia.udacitymovies.domain.Domain
import ph.codeia.udacitymovies.domain.Domain.Listing
import ph.codeia.udacitymovies.domain.MovieRepository
import ph.codeia.udacitymovies.domain.Page
import ph.codeia.udacitymovies.domain.TransientMovieCache
import javax.inject.Inject
import javax.inject.Singleton

/**
 * This file is a part of the Udacity Movies project.
 */
@Singleton
class InMemoryCache @Inject constructor(
        private val remote: MovieRepository
) : MovieRepository {

    private val cache = TransientMovieCache()

    override fun oneWithId(id: Long): Domain.Movie = when (id) {
        in cache -> cache[id]
        else -> remote.oneWithId(id).apply { cache[id] = this }
    }

    override fun firstWithTitle(title: String): Domain.Movie = when (title) {
        in cache -> cache[title]
        else -> remote.firstWithTitle(title).apply { cache[title] = this }
    }

    override fun someRecent(page: Page): List<Domain.Movie> {
        val p = Listing.CURRENT to page
        return when (p) {
            in cache -> cache[p]
            else -> remote.someRecent(page).apply { cache[p] = this }
        }
    }

    override fun somePopular(page: Page): List<Domain.Movie> {
        val p = Listing.POPULAR to page
        return when (p) {
            in cache -> cache[p]
            else -> remote.somePopular(page).apply { cache[p] = this }
        }
    }

    override fun someGood(page: Page): List<Domain.Movie> {
        val p = Listing.BEST to page
        return when (p) {
            in cache -> cache[p]
            else -> remote.someGood(page).apply { cache[p] = this }
        }
    }

}
