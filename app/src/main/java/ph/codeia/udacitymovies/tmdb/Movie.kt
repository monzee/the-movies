package ph.codeia.udacitymovies.tmdb

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import ph.codeia.udacitymovies.domain.Domain
import java.util.*

/**
 * This file is a part of the Udacity Movies project.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class Movie(
        override val id: Long,
        override val title: String,
        @JsonProperty("overview") override val synopsis: String,
        override val popularity: Float,
        @JsonProperty("vote_average") override val rating: Float,
        @JsonProperty("release_date") override val releaseDate: Date?,
        @JsonProperty("poster_path") val posterPath: String?
) : Domain.Movie {

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Paginated(
            val page: Int,
            @JsonProperty("total_pages") val totalPages: Int,
            @JsonProperty("total_results") val totalResults: Int,
            @JsonProperty("results") val payload: List<Movie>
    )


    var posterSize: Width? = null
    var thumbnailSize: Width? = null

    override val poster: Image
        get() = Image(posterPath, posterSize ?: Width.S)

    override val thumbnail: Image
        get() = Image(posterPath, thumbnailSize ?: Width.XS)

}