package ph.codeia.udacitymovies.tmdb

import ph.codeia.udacitymovies.domain.Domain
import java.net.URI

/**
 * This file is a part of the Udacity Movies project.
 */
class Image(path: String?, override val size: Domain.Size) : Domain.Image {

    companion object {
        const val MISSING_IMAGE = "//nope.png"
    }

    private val base = "http://image.tmdb.org/t/p"

    /** the double slash just before the filename is in the spec, totally intended. */
    override val location: URI by lazy {
        URI.create(path?.let { "$base/$size/$it" } ?: MISSING_IMAGE)
    }

}
