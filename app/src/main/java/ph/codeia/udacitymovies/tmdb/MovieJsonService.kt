package ph.codeia.udacitymovies.tmdb

import ph.codeia.udacitymovies.domain.Domain
import ph.codeia.udacitymovies.domain.MissingMovie
import ph.codeia.udacitymovies.domain.MovieRepository
import ph.codeia.udacitymovies.domain.Page
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * This file is a part of the Udacity Movies project.
 */
@Singleton
class MovieJsonService @Inject constructor(
        @Named("tmdb") retrofit: Retrofit
) : MovieRepository {

    interface EndPoint {
        @GET("movie/{id}")
        fun byId(@Path("id") id: Long): Call<Movie>

        @GET("search/movie")
        fun byTitle(@Query("query") query: String): Call<Movie.Paginated>

        @GET("movie/now_playing")
        fun current(@Query("page") page: Int? = null): Call<Movie.Paginated>

        @GET("movie/popular")
        fun popular(@Query("page") page: Int? = null): Call<Movie.Paginated>

        @GET("movie/top_rated")
        fun best(@Query("page") page: Int? = null): Call<Movie.Paginated>
    }

    private val service = retrofit.create(EndPoint::class.java)

    override fun oneWithId(id: Long): Domain.Movie =
            sync { byId(id) } ?: MissingMovie

    override fun firstWithTitle(title: String): Domain.Movie =
            sync { byTitle(title) }
            ?.let { it.payload.firstOrNull() }
            ?: MissingMovie

    override fun someRecent(page: Page): List<Domain.Movie> =
            sync { current(page.number + 1) }
            ?.let { it.payload }
            ?: emptyList()

    override fun somePopular(page: Page): List<Domain.Movie> =
            sync { popular(page.number + 1) }
            ?.let { it.payload }
            ?: emptyList()

    override fun someGood(page: Page): List<Domain.Movie> =
            sync { best(page.number + 1) }
            ?.let { it.payload }
            ?: emptyList()

    private inline fun <T> sync(block: MovieJsonService.EndPoint.() -> Call<T>): T? =
            block(service).execute().body()

}
