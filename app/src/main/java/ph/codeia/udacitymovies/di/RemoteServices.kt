package ph.codeia.udacitymovies.di

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import ph.codeia.udacitymovies.BuildConfig
import ph.codeia.udacitymovies.domain.MovieRepository
import ph.codeia.udacitymovies.tmdb.MovieJsonService
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 * This file is a part of the Udacity Movies project.
 */
@Module
object RemoteServices {

    @[Provides Singleton JvmStatic]
    fun jacksonMapper(): ObjectMapper = jacksonObjectMapper()

    @[Provides Named("tmdb-api-key") JvmStatic]
    fun tmdbApiKey(): String = BuildConfig.TMDB_API_KEY

    @[Provides Named("tmdb") Singleton JvmStatic]
    fun tmdbRetrofit(
            jsonMapper: ObjectMapper,
            @Named("tmdb-api-key") apiKey: String
    ): Retrofit = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .client(OkHttpClient.Builder().apply {
                addInterceptor {
                    val req = it.request()
                    val url = req.url().newBuilder()
                            .addQueryParameter("api_key", apiKey)
                            .build()
                    it.proceed(req.newBuilder().url(url).build())
                }
            }.build())
            .addConverterFactory(JacksonConverterFactory.create(jsonMapper))
            .build()

    @[Provides JvmStatic]
    fun tmdbService(service: MovieJsonService): MovieRepository = service

}
