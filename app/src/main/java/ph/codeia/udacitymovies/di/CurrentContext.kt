package ph.codeia.udacitymovies.di

import android.app.Activity
import android.app.Application
import android.content.Context
import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * This file is a part of the Udacity Movies project.
 */
@Module
class CurrentContext(@get:Provides internal val supportActivity: AppCompatActivity) {

    @get:Provides
    internal val activity: Activity = supportActivity

    @get:Provides
    internal val application: Application = supportActivity.application

    @get:[Provides Named("base")]
    internal val appContext: Context = supportActivity.applicationContext

    @get:[Provides Named("themed")]
    internal val context: Context = supportActivity

}
