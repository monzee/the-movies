package ph.codeia.udacitymovies.di

import dagger.Component
import ph.codeia.udacitymovies.domain.MovieRepository
import ph.codeia.udacitymovies.tmdb.InMemoryCache
import ph.codeia.udacitymovies.ui.index.IndexModel
import javax.inject.Singleton

/**
 * This file is a part of the Udacity Movies project.
 */
@[Singleton Component(modules = arrayOf(
        RemoteServices::class
))] interface Root {

    companion object {
        @get:JvmStatic
        val INSTANCE : Root by lazy { DaggerRoot.create() }
    }

    fun movies(): MovieRepository
    fun moviesInMemory(): InMemoryCache
    fun indexScreen(): IndexModel
}