package ph.codeia.udacitymovies.di

import dagger.Component
import ph.codeia.udacitymovies.MoviesActivity
import ph.codeia.udacitymovies.util.Per

/**
 * This file is a part of the Udacity Movies project.
 */
@[Per.Activity Component(
        dependencies = arrayOf(Root::class),
        modules = arrayOf(CurrentContext::class, MoviesActivity.Ui::class)
)] interface MoviesInjector {
    fun inject(activity: MoviesActivity)
}

