package ph.codeia.udacitymovies.domain

import java.net.URI
import java.util.*

/**
 * This file is a part of the Udacity Movies project.
 */

object Domain {

    interface Movie {
        val id: Long
        val title: String
        val poster: Image
        val thumbnail: Image
        val synopsis: String
        val rating: Float
        val popularity: Float
        val releaseDate: Date?
    }

    enum class MovieField { ID, TITLE, POSTER, THUMBNAIL, SYNOPSIS, RATING, POPULARITY, DATE }

    interface Image {
        val location: URI
        val size: Size
    }

    interface Size {
        val tier: NominalSize
        override fun toString(): String
    }

    enum class NominalSize { TINY, SMALL, MEDIUM, LARGE, XLARGE, OTHER }

    enum class Listing { CURRENT, POPULAR, BEST }

}

interface MovieProjection {
    fun withFields(fields: EnumSet<Domain.MovieField>): MovieProjection
    fun withoutFields(fields: EnumSet<Domain.MovieField>): MovieProjection
    fun withPosterSize(size: Domain.Size): MovieProjection
    fun build(): MovieRepository
}

interface MovieRepository {
    fun oneWithId(id: Long): Domain.Movie
    fun firstWithTitle(title: String): Domain.Movie
    fun someRecent(page: Page): List<Domain.Movie>
    fun somePopular(page: Page): List<Domain.Movie>
    fun someGood(page: Page): List<Domain.Movie>
}

interface MovieCache {
    operator fun contains(id: Long): Boolean
    operator fun contains(title: String): Boolean
    operator fun contains(page: Pair<Domain.Listing, Page>): Boolean
    operator fun set(id: Long, movie: Domain.Movie)
    operator fun set(title: String, movie: Domain.Movie)
    operator fun set(page: Pair<Domain.Listing, Page>, movies: List<Domain.Movie>)
    operator fun get(id: Long): Domain.Movie
    operator fun get(title: String): Domain.Movie
    operator fun get(page: Pair<Domain.Listing, Page>): List<Domain.Movie>
}


data class Page @JvmOverloads constructor(val number: Int, val size: Int = 20) {

    companion object {
        @[JvmStatic JvmOverloads]
        fun first(size: Int = 20): Page = Page(0, size)
    }

    val start = number * size
    val endExclusive: Int get() = start + size
    val next: Page by lazy { Page(number + 1, size) }

}

class MovieNotFound(
        override val message: String? = null,
        override val cause: Throwable? = null
) : NoSuchElementException(message)
