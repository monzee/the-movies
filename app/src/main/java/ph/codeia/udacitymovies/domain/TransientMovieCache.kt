package ph.codeia.udacitymovies.domain

import ph.codeia.udacitymovies.domain.Domain.Listing

/**
 * This file is a part of the Udacity Movies project.
 */
class TransientMovieCache : MovieCache {

    private val repo = TransientMovieRepository(emptyList())
    private val fetched: MutableSet<Long> = mutableSetOf()
    private val searched: MutableSet<String> = mutableSetOf()
    private val lists: MutableSet<Pair<Listing, Page>> = mutableSetOf()

    override fun contains(id: Long): Boolean = id in fetched

    override fun contains(title: String): Boolean = title in searched

    override fun contains(page: Pair<Listing, Page>): Boolean = page in lists

    override fun set(id: Long, movie: Domain.Movie) {
        fetched += id
        repo.put(movie)
    }

    override fun set(title: String, movie: Domain.Movie) {
        searched += title
        repo.put(movie)
    }

    override fun set(page: Pair<Listing, Page>, movies: List<Domain.Movie>) {
        lists += page
        movies.forEach { repo.put(it) }
    }

    override fun get(id: Long): Domain.Movie = repo.oneWithId(id)

    override fun get(title: String): Domain.Movie = repo.firstWithTitle(title)

    override fun get(page: Pair<Listing, Page>): List<Domain.Movie> {
        val (kind, p) = page
        return when (kind) {
            Listing.CURRENT -> repo.someRecent(p)
            Listing.POPULAR -> repo.somePopular(p)
            Listing.BEST -> repo.someGood(p)
        }
    }

}
