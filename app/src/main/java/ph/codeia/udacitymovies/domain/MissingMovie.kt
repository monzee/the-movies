package ph.codeia.udacitymovies.domain

import java.util.*

/**
 * This file is a part of the Udacity Movies project.
 */
object MissingMovie : Domain.Movie {

    override val id: Long
        get() = throw MovieNotFound("Tried to access the id of nothing.")

    override val title: String
        get() = throw MovieNotFound("Tried to access the title of nothing.")

    override val poster: Domain.Image
        get() = throw MovieNotFound("Tried to access the poster of nothing.")

    override val thumbnail: Domain.Image
        get() = throw MovieNotFound("Tried to access the thumbnail of nothing.")

    override val synopsis: String
        get() = throw MovieNotFound("Tried to access the synopsis of nothing.")

    override val rating: Float
        get() = throw MovieNotFound("Tried to access the rating of nothing.")

    override val popularity: Float
        get() = throw MovieNotFound("Tried to access the popularity of nothing.")

    override val releaseDate: Date
        get() = throw MovieNotFound("Tried to access the release date of nothing.")

}