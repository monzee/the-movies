package ph.codeia.udacitymovies.domain

import ph.codeia.udacitymovies.domain.Domain.Movie
import java.util.*

/**
 * This file is a part of the Udacity Movies project.
 */
class TransientMovieRepository(movies: Collection<Movie>) : MovieRepository {

    companion object {
        private val MIN_DATE = Date(Long.MIN_VALUE)
    }

    private val data: MutableMap<Long, Movie> = hashMapOf()
    private val byTitle: MutableMap<String, MutableList<Long>> = hashMapOf()

    init {
        movies.forEach { put(it) }
    }

    fun put(movie: Movie) = with(movie) {
        data[id] = this
        byTitle.getOrPut(title) { mutableListOf() } += id
    }

    override fun oneWithId(id: Long): Movie = data[id] ?: MissingMovie

    override fun firstWithTitle(title: String): Movie =
            byTitle[title]?.let { data[it.first()] } ?: MissingMovie

    override fun someRecent(page: Page): List<Movie> = data.values
            .reverseSortSlice(page.start, page.size) { it.releaseDate ?: MIN_DATE }

    override fun somePopular(page: Page): List<Movie> = data.values
            .reverseSortSlice(page.start, page.size) { it.popularity }

    override fun someGood(page: Page): List<Movie> = data.values
            .reverseSortSlice(page.start, page.size) { it.rating }

    private inline fun <T, K: Comparable<K>> Collection<T>.reverseSortSlice(
            skip: Int,
            count: Int,
            crossinline block: (T) -> K
    ): List<T> = sortedByDescending(block).drop(skip).take(count)

}
