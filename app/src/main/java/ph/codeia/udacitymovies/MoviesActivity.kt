package ph.codeia.udacitymovies

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuInflater
import dagger.Module
import dagger.Provides
import ph.codeia.udacitymovies.di.CurrentContext
import ph.codeia.udacitymovies.di.DaggerMoviesInjector
import ph.codeia.udacitymovies.di.Root
import ph.codeia.udacitymovies.domain.MovieRepository
import ph.codeia.udacitymovies.tmdb.InMemoryCache
import ph.codeia.udacitymovies.ui.index.Index
import ph.codeia.udacitymovies.ui.index.IndexPresenter
import ph.codeia.udacitymovies.ui.index.IndexView
import ph.codeia.udacitymovies.util.Job
import ph.codeia.udacitymovies.util.schedule
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named

class MoviesActivity : AppCompatActivity() {

    @Module
    object Ui {
        @[Provides JvmStatic]
        fun view(v: IndexView): Index.Display = v

        @[Provides JvmStatic]
        fun sync(p: IndexPresenter): Index.Synchronization = p

        @[Provides JvmStatic]
        fun interaction(p: IndexPresenter): Index.Interaction = p

        @[Provides Named("cached") JvmStatic]
        fun cachedMovies(r: InMemoryCache): MovieRepository = r

        @[Provides JvmStatic]
        fun scheduling(): Job.Prepare = Scheduling
        
        private object Scheduling : Job.Prepare {
            override fun <T> invoke(task: Job<T>): Job<T> = task then {
                schedule(Schedulers.io(), AndroidSchedulers.mainThread())
            }
        }
    }

    @Inject
    internal lateinit var view: Index.Display

    @Inject
    internal lateinit var presenter: Index.Synchronization

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        setSupportActionBar(findViewById(R.id.toolbar) as Toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        DaggerMoviesInjector.builder()
                .root(Root.INSTANCE)
                .currentContext(CurrentContext(this))
                .build()
                .inject(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.bind(view)
    }

    override fun onStop() {
        super.onStop()
        presenter.unbind()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        MenuInflater(this).inflate(R.menu.menu_movies, menu)
        return super.onCreateOptionsMenu(menu)
    }

}
