package ph.codeia.udacitymovies.util

import javax.inject.Scope

/**
 * This file is a part of the Udacity Movies project.
 */

object Per {
    @[Scope Retention(AnnotationRetention.RUNTIME)]
    annotation class Activity

    @[Scope Retention(AnnotationRetention.RUNTIME)]
    annotation class Goal
}
