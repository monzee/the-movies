package ph.codeia.udacitymovies.util.scheduling

/**
 * This file is a part of the Udacity Movies project.
 */
interface Runner {

    /**
     * @param task A potentially long-running function that produces
     * a single value. Use {@see Task<T>} for a little bit of composability.
     *
     * @param handler A function that receives the result or an error.
     * If the result is not null, the error is guaranteed null, but if
     * the result is null, you cannot be sure that the error is not null.
     * The task may have been successful but produced a null.
     *
     * @return A function that cancels the task when invoked
     */
    operator fun <T> invoke(task: () -> T?, handler: (T?, Throwable?) -> Unit): () -> Unit

}

