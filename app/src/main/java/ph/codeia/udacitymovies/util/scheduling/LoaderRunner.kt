package ph.codeia.udacitymovies.util.scheduling

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import ph.codeia.udacitymovies.util.Per
import java.util.concurrent.Executors
import java.util.concurrent.Future
import javax.inject.Inject

/**
 * This file is a part of the Udacity Movies project.
 */
@Per.Activity
class LoaderRunner private constructor(
        private val manager: LoaderManager,
        private val context: Context
) : Runner {

    @Inject
    constructor(activity: FragmentActivity) : this(
            activity.supportLoaderManager,
            activity.applicationContext
    )

    constructor(fragment: Fragment) : this(
            fragment.loaderManager,
            fragment.context
    )

    companion object {
        private val jobs = Executors.newCachedThreadPool()
    }

    private class Loadable<T>(
            private val load: () -> T?,
            context: Context
    ) : Loader<Pair<T?, Throwable?>>(context) {

        @Volatile private var stale = true
        private var inProgress: Future<*>? = null
        private var result: T? = null
        private var error: Throwable? = null
        private val uiHandler = Handler(context.mainLooper)

        override fun onForceLoad() {
            inProgress = inProgress ?: jobs.submit {
                try {
                    result = load()
                    stale = false
                } catch (e: Throwable) {
                    error = e
                } finally {
                    uiHandler.post {
                        deliverResult(result to error)
                        inProgress = null
                    }
                }
            }
        }

        override fun onStartLoading() = if (stale) {
            forceLoad()
        } else {
            deliverResult(result to error)
        }

        override fun onReset() {
            stale = true
            cancelLoad()
            result = null
            error = null
        }

        override fun onCancelLoad() = (inProgress?.cancel(true) ?: false).apply {
            inProgress = null
        }

    }

    /**
     * @param task Pass an instance of {@see Task<T>} with an explicit id to
     * enable loader reuse. Otherwise there's no point in using this class.
     */
    override fun <T> invoke(
            task: () -> T?,
            handler: (T?, Throwable?) -> Unit
    ): () -> Unit = when (task) {
        is Task -> task
        else -> Task(task)
    }.let {
        val id = it.id.hashCode()
        val loader = manager.initLoader(
                id,
                null,
                object : LoaderManager.LoaderCallbacks<Pair<T?, Throwable?>> {
                    override fun onLoadFinished(
                            loader: Loader<Pair<T?, Throwable?>>,
                            data: Pair<T?, Throwable?>
                    ) {
                        val (result, error) = data
                        handler(result, error)
                    }

                    override fun onLoaderReset(loader: Loader<Pair<T?, Throwable?>>) {
                    }

                    override fun onCreateLoader(id: Int, args: Bundle?) =
                            Loadable(it, context)
                })
        fun () {
            loader.cancelLoad()
            manager.destroyLoader(id)
        }
    }

}