package ph.codeia.udacitymovies.util

import rx.Scheduler
import rx.Single
import rx.Subscriber
import rx.Subscription
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * This file is a part of the Udacity Movies project.
 */
class Job<T> private constructor (private val future: Single<T>) : Subscriber<T>() {

    constructor(producer: () -> T) : this(Single.defer {
        Single.just(producer())
    })

    interface Prepare {
        operator fun <T> invoke(task: Job<T>): Job<T>
    }

    companion object {

        /**
         * Mainly for testing purposes.
         * @return T when completed, null on timeout
         * @throws Throwable Rethrows any exception thrown during the async call
         */
        @JvmStatic
        inline fun <T> toSync(
                duration: Long,
                units: TimeUnit,
                block: () -> Job<T>
        ): T? {
            val done = CountDownLatch(1)
            var value: T? = null
            var e: Throwable? = null
            val sub = block() finally { ok, err ->
                ok?.let {
                    value = it
                } ?: Unit.apply {
                    e = err
                }
                done.countDown()
            }
            done.await(duration, units)
            sub.unsubscribe()
            e?.let { throw it }
            return value
        }
    }

    object NoError : Throwable("just kidding; this is a sentinel object.")

    private lateinit var handler: (T?, Throwable) -> Unit
    private var value: T? = null

    infix fun <U> then(block: Single<T>.() -> Single<U>): Job<U> = Job(block(future))

    @JvmName("ultimately")
    infix fun finally(block: (T?, Throwable) -> Unit): Subscription {
        handler = block
        return future.subscribe(this)
    }

    override fun onCompleted() {
        handler(value, NoError)
    }

    override fun onNext(t: T) {
        value = t
    }

    override fun onError(e: Throwable?) {
        handler(null, e ?: NoError)
    }

}

fun <T> Single<T>.schedule(execute: Scheduler, listen: Scheduler): Single<T> =
        subscribeOn(execute).observeOn(listen)

