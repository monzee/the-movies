package ph.codeia.udacitymovies.util.scheduling

/**
 * This file is a part of the Udacity Movies project.
 */
class BlockingRunner : Runner {

    private companion object : () -> Unit by { Unit }

    override fun <T> invoke(
            task: () -> T?,
            handler: (T?, Throwable?) -> Unit
    ): () -> Unit = Companion.apply {
        var result: T? = null
        var error: Throwable? = null
        try {
            result = task()
        } catch (e: Throwable) {
            error = e
        } finally {
            handler(result, error)
        }
    }

}

