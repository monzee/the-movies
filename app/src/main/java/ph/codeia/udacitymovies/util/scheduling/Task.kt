package ph.codeia.udacitymovies.util.scheduling

import java.util.concurrent.atomic.AtomicInteger

/**
 * This file is a part of the Udacity Movies project.
 */
class Task<T>(
        val id: String,
        private val fn: () -> T
) : () -> T by fn {

    constructor(fn: () -> T) : this("task-${ID.andIncrement}", fn)

    constructor(value: T) : this({ value })

    companion object {
        private val ID = AtomicInteger(1)
    }

    fun <U> map(other: (T) -> U): Task<U> = Task(id) { other(fn()) }

    fun <U> bind(other: (T) -> () -> U): Task<U> = Task(id) { other(fn())() }

}