package ph.codeia.udacitymovies.util.scheduling

import rx.Scheduler
import rx.Single
import rx.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named

/**
 * This file is a part of the Udacity Movies project.
 */
class RxSingleRunner @Inject constructor(
        @Named("worker") private val worker: Scheduler,
        @Named("main") private val main: Scheduler?
) : Runner {

    constructor() : this(Schedulers.immediate())

    constructor(worker: Scheduler) : this(worker, null)

    override operator fun <T> invoke(
            task: () -> T?,
            handler: (T?, Throwable?) -> Unit
    ): () -> Unit = schedule(Single.defer { Single.just(task()) }).subscribe(
            { handler(it, null) },
            { handler(null, it) }
    ).let { { it.unsubscribe() } }

    operator fun <T> invoke(
            single: Single<T?>,
            handler: (T?, Throwable?) -> Unit
    ): () -> Unit = schedule(single).subscribe(
            { handler(it, null) },
            { handler(null, it) }
    ).let { { it.unsubscribe() } }

    fun <T> unit(value: T): Single<T> = Single.defer { Single.just(value) }

    private fun <T> schedule(single: Single<T?>): Single<T?> =
            single.subscribeOn(worker).let { s ->
                main?.let { s.observeOn(it) } ?: s
            }

}