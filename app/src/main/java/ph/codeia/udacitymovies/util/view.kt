package ph.codeia.udacitymovies.util

import android.app.Activity
import android.view.View

/**
 * This file is a part of the Udacity Movies project.
 */

inline operator fun <reified T : View> Activity.invoke(id: Int): T =
        findViewById(id) as T

inline operator fun <reified T : View> View.invoke(id: Int): T =
        findViewById(id) as T
