package ph.codeia.udacitymovies.ui.index

import ph.codeia.udacitymovies.domain.Domain.Listing
import ph.codeia.udacitymovies.domain.Page
import ph.codeia.udacitymovies.ui.index.Index.MovieTile
import java.util.*
import java.util.concurrent.FutureTask
import javax.inject.Inject
import javax.inject.Singleton

/**
 * This file is a part of the Udacity Movies project.
 */
@Singleton
class IndexModel @Inject constructor() {

    internal var visible = Listing.CURRENT

    internal val tabs: MutableMap<Listing, MutableList<MovieTile>> = mutableMapOf()

    internal val fetching = mutableMapOf<Listing, Page>()

    internal val isFetching: Boolean get() = isFetching(visible)

    internal fun isFetching(tab: Listing): Boolean = tab in fetching

    internal operator fun get(tab: Listing, page: Page): List<MovieTile> = tabs.getOrPut(tab) {
        Collections.synchronizedList(mutableListOf())
    }.apply {
        repeat(page.size + page.start - size) {
            add(MissingTile)
        }
    }

    internal var interrupted: FutureTask<Page>? = null

}