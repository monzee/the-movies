package ph.codeia.udacitymovies.ui.index

import android.app.Activity
import android.support.design.widget.TabLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.wang.avi.AVLoadingIndicatorView
import jp.wasabeef.recyclerview.animators.LandingAnimator
import ph.codeia.udacitymovies.R
import ph.codeia.udacitymovies.domain.Domain.Listing
import ph.codeia.udacitymovies.domain.Page
import ph.codeia.udacitymovies.util.invoke
import javax.inject.Inject

/**
 * This file is a part of the Udacity Movies project.
 */
class IndexView @Inject constructor(
        find: Activity,
        private val adapter: MoviesAdapter,
        private val user: Index.Interaction
) : Index.Display, TabLayout.OnTabSelectedListener {

    private val context = find.applicationContext
    private val loading: AVLoadingIndicatorView = find(R.id.is_loading)
    private val tabs = find<TabLayout>(R.id.tabs).apply {
        arrayOf("Current", "Hot", "Best").forEach { addTab(newTab().setText(it)) }
        addOnTabSelectedListener(this@IndexView)
    }

    init {
        find<RecyclerView>(R.id.list_movies).let {
            it.adapter = adapter
            it.layoutManager = GridLayoutManager(context, 2)
            it.itemAnimator = LandingAnimator().apply {
                addDuration = 175
                removeDuration = 75
            }
        }
    }

    override fun onTabReselected(tab: TabLayout.Tab) {
        // refresh?
    }

    override fun onTabUnselected(tab: TabLayout.Tab) {
    }

    override fun onTabSelected(tab: TabLayout.Tab) {
        user.didSwitchTab(tab.choose(Listing.CURRENT, Listing.POPULAR, Listing.BEST))
    }

    override fun show(movie: Index.MovieTile) {
        tell("showing movie #${movie.id}: ${movie.title}.")
    }

    override fun show(tab: Listing) {
        val i = tab.index
        // select() needs to be called in order to set the active tab button but
        // onTabSelected() will not be triggered by select() if the index
        // does not change (it will trigger onTabReselected() instead)
        when (tabs.selectedTabPosition) {
            i -> user.didSwitchTab(tab)
            else -> tabs.getTabAt(i)?.select()
        }
    }

    override fun tell(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun toggleSpinner(visible: Boolean) = when {
        visible -> loading.smoothToShow()
        else -> loading.hide()
    }

    override fun prepare(items: List<Index.MovieTile>) {
        Log.d("mz", "prepare: getting ${items.size} items.")
        adapter.items = items
    }

    override fun done(page: Page) {
        Log.d("mz", "done")
        toggleSpinner(false)
        val count = adapter.itemCount
        when {
            page.size == 0 -> tell("Empty page?")
            page.start < count -> {
                val intersection = count - page.start
                adapter.notifyItemRangeChanged(page.start, intersection)
                if (page.size > intersection) {
                    adapter.notifyItemRangeInserted(count, page.size - intersection)
                }
            }
            // TODO: handle gaps in the list
            // this acts as if the page size is actually bigger than stated.
            // i.e. it assumes that the gap between the last element and the
            // page start is valid and included properly before this method was called
            else -> adapter.notifyItemRangeInserted(count, page.size + page.start - count)
        }
    }

    private fun <T> TabLayout.Tab.choose(vararg choices: T): T = choices[position]

    private val Listing.index: Int get() = when (this) {
        Listing.CURRENT -> 0
        Listing.POPULAR -> 1
        Listing.BEST -> 2
    }

}