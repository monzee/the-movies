package ph.codeia.udacitymovies.ui.index

import ph.codeia.udacitymovies.domain.Domain.Listing
import ph.codeia.udacitymovies.domain.Domain.Movie
import ph.codeia.udacitymovies.domain.MovieNotFound
import ph.codeia.udacitymovies.domain.Page
import java.net.URI

/**
 * This file is a part of the Udacity Movies project.
 */

object Index {

    interface Display {
        fun show(movie: MovieTile)
        fun show(tab: Listing)
        fun tell(message: String)
        fun toggleSpinner(visible: Boolean)
        fun prepare(items: List<MovieTile>)
        fun done(page: Page)
    }

    interface Interaction {
        fun didChooseTile(index: Int)
        fun didSwitchTab(tab: Listing)
    }

    interface Synchronization {
        fun bind(view: Display)
        fun unbind()
        fun refresh()
        fun fetchMovies(tab: Listing)
        fun moviesFetched(tab: Listing, items: List<Movie>)
    }

    interface MovieTile {
        val id: Long
        val title: String
        val image: URI
    }

}

data class Tile(
        override val id: Long,
        override val title: String,
        override val image: URI
) : Index.MovieTile

object MissingTile : Index.MovieTile {
    override val id: Long
        get() = throw MovieNotFound()

    override val title: String
        get() = throw MovieNotFound()

    override val image: URI
        get() = throw MovieNotFound()
}
