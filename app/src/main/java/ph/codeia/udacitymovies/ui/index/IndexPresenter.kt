package ph.codeia.udacitymovies.ui.index

import ph.codeia.udacitymovies.domain.Domain
import ph.codeia.udacitymovies.domain.Domain.Listing
import ph.codeia.udacitymovies.domain.MovieRepository
import ph.codeia.udacitymovies.domain.Page
import ph.codeia.udacitymovies.util.Job
import ph.codeia.udacitymovies.util.Per
import java.util.concurrent.FutureTask
import javax.inject.Inject

/**
 * This file is a part of the Udacity Movies project.
 */
@Per.Activity
class IndexPresenter @Inject constructor(
        private val state: IndexModel,
        private val movies: MovieRepository,
        private val schedule: Job.Prepare
) : Index.Interaction, Index.Synchronization {

    private var view: Index.Display? = null

    override fun didChooseTile(index: Int) {
        state.tabs[state.visible]?.let {
            view?.show(it[index])
        }
    }

    override fun didSwitchTab(tab: Listing) {
        state.visible = tab
        view?.let {
            state.tabs[tab]?.let { items ->
                it.prepare(items)
                when (state.interrupted) {
                    is Any -> it.toggleSpinner(true)
                    else -> it.done(Page.first(items.size))
                }
            } ?: fetchMovies(tab)
        }
    }

    override fun bind(view: Index.Display) {
        require(this.view == null) { "Unbind first before rebinding!" }
        this.view = view
        view.show(state.visible)
        state.interrupted?.let {
            schedule(Job {
                it.get()
            }) finally { ok, err ->
                ok?.let { this.view?.done(it) }
                state.interrupted = null
            }
        }
    }

    override fun unbind() {
        view = null
        with(state) {
            fetching[visible]?.let {
                interrupted?.cancel(false)
                interrupted = FutureTask { it }
            }
        }
    }

    override fun refresh() {
        TODO("refresh")
    }

    override fun fetchMovies(tab: Listing) {
        if (state.isFetching(tab)) {
            return
        }
        view?.let {
            val page = Page.first()
            state.fetching[tab] = page
            it.toggleSpinner(true)
            it.prepare(state[tab, page])
            schedule(Job {
                when (tab) {
                    Listing.CURRENT -> movies.someRecent(page)
                    Listing.POPULAR -> movies.somePopular(page)
                    Listing.BEST -> movies.someGood(page)
                }
            }) finally { ok, err ->
                ok?.let { moviesFetched(tab, it) } ?: report(err)
            }
        }
    }

    override fun moviesFetched(tab: Listing, items: List<Domain.Movie>) {
        state.fetching[tab]?.let { page ->
            state.tabs[tab]?.let { tiles ->
                (page.start until page.endExclusive).zip(items).forEach {
                    val (i, m) = it
                    tiles[i] = Tile(m.id, m.title, m.poster.location)
                }
            }
            state.fetching.remove(tab)
            view?.done(page) ?: state.interrupted?.let {
                when (state.visible) {
                    tab -> it.run()
                    else -> it.cancel(false)
                }
            }
        }
    }

    private fun report(error: Throwable) = when (error) {
        is Job.NoError -> view?.tell("Service returned nothing! Invalid API key?")
        else -> {
            error.printStackTrace()
            view?.tell(error.message ?: "Cannot reach service!")
        }
    }

}
