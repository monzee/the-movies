package ph.codeia.udacitymovies.ui.index

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.xgc1986.ripplebutton.utils.RippleDrawableHelper
import ph.codeia.udacitymovies.R
import ph.codeia.udacitymovies.util.invoke
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * This file is a part of the Udacity Movies project.
 */
class MoviesAdapter @Inject constructor(
        private val user: Index.Interaction,
        activity: AppCompatActivity
) : RecyclerView.Adapter<MoviesAdapter.Item>() {

    class Item(
            parent: View,
            private val picasso: Picasso
    ) : RecyclerView.ViewHolder(parent) {

        private val image: ImageView = parent(R.id.the_thumbnail)
        private val title: TextView = parent(R.id.the_title)
        internal val target = parent<View>(R.id.do_choose).apply {
            val color = 0x40ffffff
            background = RippleDrawableHelper.createRippleDrawable(this, color)
        }

        fun show(item: Index.MovieTile) = when (item) {
            !is MissingTile -> {
                target.isClickable = true
                title.text = item.title
                picasso.load(item.image.toASCIIString())
                        .noFade()
                        .into(image)
            }
            else -> Unit.apply {
                target.isClickable = false
                // show placeholder image
            }
        }
    }

    init {
        registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                count = itemCount + positionStart
            }
        })
    }

    private val inflater = LayoutInflater.from(activity)
    private val picasso = Picasso.with(activity.baseContext)
    private var count: Int = 0

    internal var items: List<Index.MovieTile> by Delegates.observable(emptyList()) {
        prop, old, new -> if (old !== new) {
            count = 0
            notifyItemRangeRemoved(0, old.size)
        }
    }

    override fun getItemCount(): Int = count

    override fun onBindViewHolder(holder: Item, position: Int) {
        holder.show(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        return inflater.inflate(R.layout.item_movie, parent, false).let {
            Item(it, picasso).apply {
                target.setOnClickListener {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        user.didChooseTile(adapterPosition)
                    }
                }
            }
        }
    }

}
