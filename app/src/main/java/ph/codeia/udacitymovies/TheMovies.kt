package ph.codeia.udacitymovies

import android.app.Application
import com.squareup.leakcanary.LeakCanary

/**
 * This file is a part of the Udacity Movies project.
 */
class TheMovies : Application() {

    override fun onCreate() {
        super.onCreate()
        LeakCanary.install(this)
    }

}