package ph.codeia.udacitymovies

import org.junit.Assert.*
import org.junit.Test
import ph.codeia.udacitymovies.util.Job
import ph.codeia.udacitymovies.util.schedule
import rx.Scheduler
import rx.Single
import rx.exceptions.OnErrorFailedException
import rx.schedulers.Schedulers
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import org.junit.Assert.fail as f

/**
 * This file is a part of the Udacity Movies project.
 */
class JobTest {

    companion object {
        val ITEMS = mapOf(
                409122 to mapOf("title" to "Scooby-Doo! And WWE: Curse of the Speed Demon")
        )
        val BG: Scheduler = Schedulers.computation()
        val FG: Scheduler = Schedulers.immediate()
    }

    @Test
    fun not_actually_async_unless_you_set_the_schedulers() {
        var called = false
        val main = Thread.currentThread()
        Job {
            assertSame("different threads.", main, Thread.currentThread())
        } finally { ok, err ->
            assertSame("different threads.", main, Thread.currentThread())
            called = true
        }
        assertTrue("nothing actually happened.", called)
    }

    @Test
    fun now_it_really_is_async() {
        val main = Thread.currentThread()
        val done = CountDownLatch(1)
        Job {
            assertNotSame("same threads", main, Thread.currentThread())
        } then {
            assertSame("different threads", main, Thread.currentThread())
            subscribeOn(BG).observeOn(FG)
        } finally { ok, err ->
            assertNotSame("same threads", main, Thread.currentThread())
            done.countDown()
        }
        assertTrue(done.await(1, TimeUnit.SECONDS))
    }

    @Test
    fun sugar_free_change_schedulers() {
        val main = Thread.currentThread()
        val done = CountDownLatch(1)
        Single.defer {
            assertNotSame(main, Thread.currentThread())
            Single.just("nope.")
        }.subscribeOn(BG).subscribe {
            // just changing the subscribe thread also causes the obs thread to
            // be called in a non-main thread. makes sense actually. the main
            // thread can't possibly watch the background thread, otherwise it's
            // not async.
            assertNotSame(main, Thread.currentThread())
            done.countDown()
        }
        assertTrue(done.await(1, TimeUnit.SECONDS))
    }

    @Test
    fun get_one() {
        val done = CountDownLatch(1)
        Job {
            ITEMS[409122]!!
        } then {
            schedule(BG, FG)
        } finally { ok, err ->
            ok?.let {
                assertEquals("Scooby-Doo! And WWE: Curse of the Speed Demon", it["title"])
                done.countDown()
            } ?: f("check your fixtures")
        }
        assertTrue("timeout", done.await(1, TimeUnit.SECONDS))
    }

    @Test
    fun get_one_then_project() {
        val done = CountDownLatch(1)
        Job {
            ITEMS[409122]!!
        } then {
            schedule(BG, FG).map { it["title"] }
        } finally { ok, err ->
            ok?.let {
                assertEquals("Scooby-Doo! And WWE: Curse of the Speed Demon", it)
                done.countDown()
            } ?: f("check your fixtures")
        }
        assertTrue("timeout", done.await(1, TimeUnit.SECONDS))
    }

    @Test
    fun get_one_artificial_delay() {
        val done = CountDownLatch(1)
        val sub = Job {
            ITEMS[409122]!!
        } then {
            schedule(BG, FG)
                .delay(1, TimeUnit.SECONDS)
                .map { it["title"] }
        } finally { ok, err ->
            f("should timeout and not reach this")
        }
        assertFalse("did not timeout", done.await(50, TimeUnit.MILLISECONDS))
        // if i don't do this, the subscriber will be invoked eventually
        sub.unsubscribe()
    }

    @Test
    fun unsub_while_getting() {
        val done = CountDownLatch(1)
        val sub = Job {
            ITEMS[409122]!!
        } then {
            schedule(BG, FG)
                .map { it["title"] }
                .delay(1, TimeUnit.SECONDS)
                .doOnUnsubscribe { done.countDown() }
        } finally { ok, err ->
            f("should be unreachable")
        }
        sub.unsubscribe()
        assertTrue("timeout", done.await(500, TimeUnit.MILLISECONDS))
    }

    class AsyncError(message: String = "async error") : RuntimeException(message)

    @Test(expected = OnErrorFailedException::class)
    fun error_condition_sync() {
        Job {
            throw AsyncError()
        } finally { ok, err ->
            throw err
        }
    }

    @Test(expected = AsyncError::class)
    fun error_condition_async() {
        val done = CountDownLatch(1)
        var e: Throwable? = null
        Job { throw AsyncError() }
        .then { schedule(BG, FG) }
        .finally { ok, err ->
            e = err
            done.countDown()
        }
        assertTrue(done.await(1, TimeUnit.SECONDS))
        assertNotNull(e)
        throw e!!
    }

    @Test(expected = AsyncError::class)
    fun error_in_then() {
        Job { "yes" }
        // then is synchronous, so this throws even without attaching a subscriber
        .then<Any> { throw AsyncError() }
    }

    @Test(timeout = 200)
    fun subscribing_to_a_completed_future_does_not_recompute_the_value() {
        val c = AtomicInteger(100)
        val n = Job {
            c.andIncrement
        } then {
            delay(50, TimeUnit.MILLISECONDS)
        }
        n finally { ok, err -> assertEquals(100, ok!!) }
        Thread.sleep(100)  // this test is really flaky when under heavy cpu load
        repeat(500) {
            n finally { ok, err -> assertEquals(100, ok!!) }
        }
    }

}
