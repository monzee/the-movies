package ph.codeia.udacitymovies;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * This file is a part of the UdacityMovies project.
 */
public class Util {

    public static URI uriTo(String resource) throws URISyntaxException {
        return Util.class.getClassLoader().getResource(resource).toURI();
    }

    public static InputStream inputStreamOf(String filename) {
        return Util.class.getClassLoader().getResourceAsStream(filename);
    }

}
