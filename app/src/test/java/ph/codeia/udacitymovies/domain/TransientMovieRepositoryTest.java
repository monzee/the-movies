package ph.codeia.udacitymovies.domain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.kotlin.KotlinModule;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import ph.codeia.udacitymovies.Util;
import ph.codeia.udacitymovies.tmdb.Movie;
import ph.codeia.udacitymovies.util.Job;
import rx.Single;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static ph.codeia.udacitymovies.util.AsyncKt.schedule;

/**
 * This file is a part of the Udacity Movies project.
 */
public class TransientMovieRepositoryTest {
    private static List<Movie> DATA;
    private TransientMovieRepository repo;

    @BeforeClass
    public static void readJson() throws IOException, URISyntaxException {
        ObjectMapper fromJson = new ObjectMapper().registerModule(new KotlinModule());
        DATA = fromJson.readValue(
                new File(Util.uriTo("payload-now-playing.json")),
                new TypeReference<List<Movie>>() {}
        );
    }

    @Before
    public void setup() {
        repo = new TransientMovieRepository(DATA);
    }

    @Test
    public void fetch_one_by_id() {
        Domain.Movie m = repo.oneWithId(409122);
        assertEquals("Scooby-Doo! And WWE: Curse of the Speed Demon", m.getTitle());
    }

    @Test
    public void first_with_unique_title() {
        Domain.Movie m = repo.firstWithTitle("Jason Bourne");
        assertEquals("Jason Bourne", m.getTitle());
        assertEquals(324668, m.getId());
    }

    @Test
    public void first_with_common_title() {
        Domain.Movie m = repo.firstWithTitle("Emerald Green");
        assertEquals(1234567, m.getId());
        assertThat(m.getSynopsis(), containsString("copy-pasted from #337556"));
        Domain.Movie other = repo.oneWithId(337556);
        assertNotSame(m, other);
        assertEquals(m.getTitle(), other.getTitle());
    }

    @Test
    public void ordered_by_popularity() {
        float last = Float.POSITIVE_INFINITY;
        int count = 0;
        for (Domain.Movie m : repo.somePopular(new Page(0, 10))) {
            assertTrue(
                    "popularity is not monotonically decreasing.",
                    Float.compare(last, m.getPopularity()) >= 0
            );
            last = m.getPopularity();
            count++;
        }
        assertEquals(10, count);
    }

    @Test
    public void ordered_by_popularity_second_page() {
        float last = Float.POSITIVE_INFINITY;
        int count = 0;
        for (Domain.Movie m : repo.somePopular(new Page(1, 10))) {
            assertTrue(
                    "popularity is not monotonically decreasing.",
                    Float.compare(last, m.getPopularity()) >= 0
            );
            last = m.getPopularity();
            count++;
        }
        assertEquals(10, count);
    }


    @Test
    public void ordered_by_rating() {
        float last = Float.POSITIVE_INFINITY;
        int count = 0;
        for (Domain.Movie m : repo.someGood(new Page(0, 20))) {
            assertTrue(
                    "rating is not monotonically decreasing.",
                    Float.compare(last, m.getRating()) >= 0
            );
            last = m.getRating();
            count++;
        }
        assertEquals(20, count);
    }

    @Test
    public void ordered_by_date() {
        Date last = null;
        for (Domain.Movie m : repo.someRecent(new Page(0, 20))) {
            if (last == null) {
                last = m.getReleaseDate();
                continue;
            }
            assertFalse(
                    "release date is not monotonically decreasing.",
                    m.getReleaseDate().after(last)
            );
            last = m.getReleaseDate();
        }
    }

    @Test
    public void async_get_one() {
        final CountDownLatch done = new CountDownLatch(1);
        new Job<>(new Function0<Domain.Movie>() {
            @Override
            public Domain.Movie invoke() {
                return repo.oneWithId(324668);
            }
        }).then(new Function1<Single<Domain.Movie>, Single<String>>() {
            @Override
            public Single<String> invoke(Single<Domain.Movie> movieSingle) {
                return schedule(movieSingle, Schedulers.computation(), Schedulers.immediate())
                        .delay(50, TimeUnit.MILLISECONDS)
                        .map(new Func1<Domain.Movie, String>() {
                            @Override
                            public String call(Domain.Movie movie) {
                                return movie.getTitle();
                            }
                        });
            }
        }).ultimately(new Function2<String, Throwable, Unit>() {
            @Override
            public Unit invoke(String s, Throwable throwable) {
                assertEquals("Jason Bourne", s);
                done.countDown();
                return null;
            }
        });
        try {
            assertTrue("timeout", done.await(1, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            fail("interrupted");
        }
    }
}
