package ph.codeia.udacitymovies;

import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void can_read_files() throws URISyntaxException {
        File f = new File(getClass().getResource("/payload-now-playing.json").toURI());
        assertTrue(f.canRead());
    }
}