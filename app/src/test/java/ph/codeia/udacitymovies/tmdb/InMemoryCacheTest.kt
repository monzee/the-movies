package ph.codeia.udacitymovies.tmdb

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import ph.codeia.udacitymovies.domain.Domain
import ph.codeia.udacitymovies.domain.MissingMovie
import ph.codeia.udacitymovies.domain.MovieRepository
import ph.codeia.udacitymovies.domain.TransientMovieRepository

/**
 * This file is a part of the Udacity Movies project.
 */
class InMemoryCacheTest {

    private data class PartialMovie(
            override val id: Long,
            override val title: String
    ) : Domain.Movie by MissingMovie

    private class Spy(
            val delegate: TransientMovieRepository
    ) : MovieRepository by delegate {
        enum class M { oneWithId, firstWithTitle, someRecent, somePopular, someGood }
        val calls: MutableMap<M, Int> = mutableMapOf()

        override fun oneWithId(id: Long): Domain.Movie {
            calls[M.oneWithId] = calls[M.oneWithId]?.let { it + 1 } ?: 1
            return delegate.oneWithId(id)
        }
    }

    private val remote = Spy(TransientMovieRepository(emptyList()))
    private val cache = InMemoryCache(remote)

    @Before
    fun setup() {
        with(remote.delegate) {
            put(PartialMovie(1, "rashomon"))
            put(PartialMovie(2, "man from lamancha"))
            put(PartialMovie(3, "soylent green"))
            put(PartialMovie(4, "kramer vs. kramer"))
            put(PartialMovie(5, "oddball"))
        }
    }

    @Test
    fun fetches_objects_from_remote() {
        assertSame(cache.oneWithId(1), remote.oneWithId(1))
    }

    @Test
    fun does_not_refetch_objects() {
        assertTrue(Spy.M.oneWithId !in remote.calls)
        val m = cache.oneWithId(3)
        assertEquals(1, remote.calls[Spy.M.oneWithId])
        val n = cache.oneWithId(3)
        assertSame(m, n)
        assertEquals("soylent green", n.title)
        assertEquals(1, remote.calls[Spy.M.oneWithId])
    }
}