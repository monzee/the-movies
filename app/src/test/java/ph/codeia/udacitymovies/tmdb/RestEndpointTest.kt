package ph.codeia.udacitymovies.tmdb

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.Assert.*
import org.junit.Assert.fail as f
import org.junit.Test
import ph.codeia.udacitymovies.BuildConfig
import ph.codeia.udacitymovies.di.RemoteServices
import ph.codeia.udacitymovies.domain.Page
import ph.codeia.udacitymovies.util.Job
import java.util.concurrent.TimeUnit

/**
 * This file is a part of the Udacity Movies project.
 */
class RestEndpointTest {

    companion object {
        @JvmStatic
        private val repo = MovieJsonService(
                RemoteServices.tmdbRetrofit(jacksonObjectMapper(), BuildConfig.TMDB_API_KEY)
        )
    }

    @Test
    fun get_one() {
        val m = repo.oneWithId(409122)
        assertEquals("Scooby-Doo! And WWE: Curse of the Speed Demon", m.title)
    }

    @Test
    fun get_one_async() {
        try {
            Job.toSync(5, TimeUnit.SECONDS) {
                Job { repo.oneWithId(324668) } then { map { it.title } }
            }
            ?.let { assertEquals("Jason Bourne", it) }
            ?: f("timeout")
        } catch (e: Throwable) {
            println(e)
            f("network error?")
        }
    }

    @Test
    fun first_by_title() {
        val m = repo.firstWithTitle("Teenage Mutant Ninja Turtles")
        assertEquals("Teenage Mutant Ninja Turtles", m.title)
    }

    @Test
    fun current() {
        val ms = repo.someRecent(Page.first())
        assertEquals(20, ms.size)
        ms.forEach { assertNotNull(it) }
    }

    @Test
    fun popular() {
        val ms = repo.somePopular(Page.first())
        assertEquals(20, ms.size)
        ms.forEach { assertNotNull(it) }
    }

    @Test
    fun best() {
        val ms = repo.someGood(Page.first())
        assertEquals(20, ms.size)
        ms.forEach { assertNotNull(it) }
    }
}