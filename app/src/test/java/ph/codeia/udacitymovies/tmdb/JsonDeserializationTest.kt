package ph.codeia.udacitymovies.tmdb

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import ph.codeia.udacitymovies.Util
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * This file is a part of the Udacity Movies project.
 */
class JsonDeserializationTest {

    val fromJson = jacksonObjectMapper()

    @Test
    fun deserialize_single_entity() {
        with(fromJson.readValue(File(Util.uriTo("single.json")), Movie::class.java)) {
            assertEquals(297761, id)
            assertEquals("Suicide Squad", title)
            SimpleDateFormat("yyyy-MM-dd", Locale.US).let {
                it.timeZone = StdDateFormat.getDefaultTimeZone()
                assertEquals(it.parse("2016-08-03"), releaseDate)
            }
            assertEquals(49.773978f, popularity, 0.00001f)
            assertEquals(6.1f, rating, 0.00001f)
            assertEquals("/t/p/w92//e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", thumbnail.location.path)
            assertEquals("/t/p/w154//e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", poster.location.path)
            assertEquals("From DC Comics comes the Suicide Squad, an antihero team of " +
                    "incarcerated supervillains who act as deniable assets for the United " +
                    "States government, undertaking high-risk black ops missions in exchange " +
                    "for commuted prison sentences.",
                    synopsis
            );
        }
    }

    @Test
    fun entries_in_a_list() {
        val listOfMovies = object : TypeReference<List<Movie>>() {}
        val jsonFile = File(Util.uriTo("payload-now-playing.json"))
        with(fromJson.readValue<List<Movie>>(jsonFile, listOfMovies)) {
            assertEquals(21, size)
            zip(listOf(
                    1234567L to "Emerald Green",
                    297761L to "Suicide Squad",
                    324668L to "Jason Bourne",
                    188927L to "Star Trek Beyond",
                    43074L to "Ghostbusters",
                    382322L to "Batman: The Killing Joke",
                    390989L to "Sharknado 4: The 4th Awakens",
                    345911L to "Lights Out",
                    376659L to "Bad Moms",
                    353571L to "Tallulah",
                    407375L to "Sniper: Ghost Shooter",
                    337556L to "Emerald Green",
                    407518L to "Chosen",
                    322240L to "Nine Lives",
                    328387L to "Nerve",
                    294272L to "Pete's Dragon",
                    410144L to "Black Widows",
                    403119L to "In the Deep",
                    409122L to "Scooby-Doo! And WWE: Curse of the Speed Demon",
                    352186L to "Good Kids",
                    397395L to "Batman Unveiled"
            )).forEach {
                val (actual, expected) = it
                val (id, title) = expected
                assertEquals(id, actual.id)
                assertEquals(title, actual.title)
            }
        }
    }

    @Test
    fun paginated_server_response() {
        // why is this test so slow?
        val jsonFile = File(Util.uriTo("paginated-now-playing.json"))
        with(fromJson.readValue(jsonFile, Movie.Paginated::class.java)) {
            assertEquals(1, page)
            assertEquals(1560, totalResults)
            assertEquals(totalResults / 20, totalPages)
            assertEquals(20, payload.size)
            payload.forEach { assertNotNull(it) }
        }
    }

}
