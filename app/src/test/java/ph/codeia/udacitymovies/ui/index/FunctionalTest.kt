package ph.codeia.udacitymovies.ui.index

import org.junit.Assert.*
import org.junit.Test
import ph.codeia.udacitymovies.domain.*
import ph.codeia.udacitymovies.domain.Domain.Listing
import ph.codeia.udacitymovies.ui.index.Index.MovieTile
import ph.codeia.udacitymovies.util.Job
import rx.schedulers.Schedulers
import java.net.URI
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger

/**
 * This file is a part of the Udacity Movies project.
 */
class FunctionalTest {

    val repo = TransientMovieRepository(emptyList())

    private companion object {
        val daily: Calendar get() =
            Calendar.getInstance().apply { add(Calendar.DAY_OF_MONTH, c.andIncrement) }
        val c = AtomicInteger(0)
    }

    private class M(
            override val id: Long,
            override val title: String,
            override val poster: Domain.Image
    ) : Domain.Movie by MissingMovie {
        override val releaseDate: Date by lazy { daily.time }
    }

    private class I(path: String) : Domain.Image {
        override val location: URI = URI(path)
        override val size: Domain.Size = object : Domain.Size {
            override val tier = Domain.NominalSize.TINY
            override fun toString(): String = ""
        }
    }

    private class View(private val user: Index.Interaction) : Index.Display {
        lateinit var items: List<MovieTile>
        lateinit var chosen: MovieTile
        var spinning: Boolean = false
        var active: Listing = Listing.CURRENT

        override fun show(movie: MovieTile) {
            chosen = movie
            println("showing movie #${movie.id} | ${movie.title}")
        }

        override fun show(tab: Listing) {
            println("showing tab $tab")
            active = tab
            user.didSwitchTab(tab)
        }

        override fun tell(message: String) {
            println(message)
        }

        override fun toggleSpinner(visible: Boolean) {
            if (spinning != visible) {
                println("i ${if (visible) "am" else "stopped"} spinning.")
            }
            spinning = visible
        }

        override fun prepare(items: List<MovieTile>) {
            this.items = items
            println("got ${items.size}-item list")
        }

        override fun done(page: Page) {
            toggleSpinner(false)
            println("updated items from ${page.start} to ${page.endExclusive - 1}")
        }
    }

    val sync = object : Job.Prepare {
        override fun <T> invoke(task: Job<T>): Job<T> = task
    }

    @Test
    fun bind_typical_cold_start() {
        val m = IndexModel()
        val p = IndexPresenter(m, repo, sync)
        val v = View(p)
        with (repo) {
            (0 until 4).map{ it * 5L }.forEach {
                put(M(it + 1, "guilty crown", I("/a.jpg")))
                put(M(it + 2, "aldnoah zero", I("/b.jpg")))
                put(M(it + 3, "xenosaga", I("/c.jpg")))
                put(M(it + 4, "kill la kill", I("/d.jpg")))
                put(M(it + 5, "gundam unicorn", I("/e.jpg")))
            }
        }
        p.bind(v)
        v.items.forEach { assertNotSame(it, MissingTile) }
        assertEquals(20, v.items.size)
        assertFalse(m.isFetching)
    }

    @Test
    fun cached_tiles_are_replaced_after_returning_from_async_call() {
        val m = IndexModel().apply {
            fetching[Listing.CURRENT] = Page.first(20)
            tabs[Listing.CURRENT] = (0 until 20).map { MissingTile }.toMutableList<MovieTile>()
        }
        val p = IndexPresenter(m, repo, sync)
        p.moviesFetched(Listing.CURRENT, mutableListOf<Domain.Movie>().apply {
            (0 until 4).map{ it * 5L }.forEach {
                add(M(it + 1, "guilty crown", I("/a.jpg")))
                add(M(it + 2, "aldnoah zero", I("/b.jpg")))
                add(M(it + 3, "xenosaga", I("/c.jpg")))
                add(M(it + 4, "kill la kill", I("/d.jpg")))
                add(M(it + 5, "gundam unicorn", I("/e.jpg")))
            }
        })
        m.tabs[Listing.CURRENT]!!.forEach {
            assertNotSame(it, MissingTile)
        }
        assertFalse(m.isFetching(Listing.CURRENT))
    }

    @Test
    fun first_get_produces_a_list_of_missing_tiles_as_big_as_the_page() {
        val page = Page.first()
        val m = IndexModel()
        val tiles = m[Listing.CURRENT, page]
        assertEquals(page.size, tiles.size)
        assertTrue(tiles.all { it is MissingTile })
    }

    @Test
    fun active_tab_restored_on_restart() {
        val m = IndexModel()
        val p = IndexPresenter(m, repo, sync)
        val v = View(p)
        p.bind(v)
        p.didSwitchTab(Listing.POPULAR)
        p.unbind()

        val pLand = IndexPresenter(m, repo, sync)
        val vLand = View(pLand)
        assertEquals(Listing.CURRENT, vLand.active)
        pLand.bind(vLand)
        assertEquals(Listing.POPULAR, vLand.active)
        assertSame(vLand.items, m.tabs[vLand.active])
    }

    class SignalAfter(val latch: CountDownLatch) : Job.Prepare {
        override fun <T> invoke(task: Job<T>): Job<T> = task then {
            subscribeOn(Schedulers.io())
            .observeOn(Schedulers.immediate())
            .doAfterTerminate { latch.countDown() }
        }
    }

    class WaitBefore(
            val method: Listing,
            val latch: CountDownLatch,
            val delegate: MovieRepository
    ) : MovieRepository by delegate {
        override fun someRecent(page: Page): List<Domain.Movie> {
            if (method == Listing.CURRENT) {
                latch.await()
            }
            return delegate.someRecent(page)
        }

        override fun somePopular(page: Page): List<Domain.Movie> {
            if (method == Listing.POPULAR) {
                latch.await()
            }
            return delegate.somePopular(page)
        }

        override fun someGood(page: Page): List<Domain.Movie> {
            if (method == Listing.BEST) {
                latch.await()
            }
            return delegate.someGood(page)
        }
    }

    class Checkpoint(val pause: CountDownLatch, val resume: CountDownLatch) {
        fun clear() {
            pause.countDown()
            resume.await()
        }
    }

    @Test(timeout = 1000)
    fun can_resume_when_unbound_during_fetch() {
        val check = Checkpoint(CountDownLatch(1), CountDownLatch(1))
        val counter = AtomicInteger(0)
        val r = WaitBefore(Listing.CURRENT, check.pause, repo)
        val pausedRepo = object : MovieRepository by r {
            override fun someRecent(page: Page): List<Domain.Movie> =
                    r.someRecent(page).apply { counter.andIncrement }
        }
        val m = IndexModel()
        val p = IndexPresenter(m, pausedRepo, SignalAfter(check.resume))
        val v = View(p)
        p.bind(v)
        assertTrue(m.isFetching)
        assertTrue(v.spinning)
        assertNull(m.interrupted)
        p.unbind()
        check.clear()
        assertFalse("the fetch should have completed by now", m.isFetching)
        assertTrue("should still be spinning because nobody's around to reset it",
                v.spinning)
        assertNotNull(m.interrupted)

        // restart after rotation
        val pLand = IndexPresenter(m, pausedRepo, sync)
        val vLand = View(pLand)
        val future = m.interrupted!!
        pLand.bind(vLand)
        assertEquals("shouldn't call fetch again", 1, counter.get())
        assertSame(vLand.items, m.tabs[m.visible])
        assertTrue(future.isDone)
        assertFalse(future.isCancelled)
    }

    @Test(timeout = 1000)
    fun doesnt_continue_from_interruption_when_active_tab_changes_before_completion() {
        val check = Checkpoint(CountDownLatch(1), CountDownLatch(2))
        val pausedRepo = WaitBefore(Listing.CURRENT, check.pause, repo)
        val m = IndexModel()
        val p = IndexPresenter(m, pausedRepo, SignalAfter(check.resume))
        val v = View(p)
        p.bind(v)
        p.unbind()
        val future = m.interrupted!!
        val nextP = IndexPresenter(m, repo, SignalAfter(check.resume))
        val nextV = View(nextP)
        nextP.bind(nextV)
        nextV.show(Listing.BEST)
        check.clear()
        assertTrue(future.isCancelled)
    }
}